const express = require('express')
const fetch = require('node-fetch')

const app = express()

app.set('json spaces', 2)

const local = false

const port = !local ? 8080 : 3000

const remote_host = 'node-tunnel.opensas.now.sh'

const now_destination = `https://node-tunnel.opensas.now.sh/from_tunnel`
const local_destination = `http://localhost:${port}/from_tunnel`

let codes = []

const isNow = (req) => req.headers.host === remote_host
const getDestination = (req) => isNow(req) ? now_destination : local_destination

app.get('/from_meli', async (req, res, next) => {

  try {
    console.log({ 'url': req.url, 'query': req.query })

    const code = req.query.code
    const destination = getDestination(req)
    const url = `${destination}?code=${code}`

    console.log(`about to fetch ${url}`)

    const resp = await fetch(url)
    const tunnel_response = await resp.json()

    console.log('!', {tunnel_response})
    console.log('!!' , {codes})
    res.json({ code, destination, url, tunnel_response })
  } catch(e) {
    next(e)
  }
})

app.get('/from_tunnel', (req, res) => {
  console.log({ 'url': req.url, 'query': req.query })
  console.log(`about to push code ${req.query.code}`)

  // codes.push(req.query.code)
  codes = [...codes, req.query.code]
  console.log('!!!' , {codes})

  res.json(codes)
})

app.get('/codes', (req, res) => {
  console.log({ 'url': req.url, 'query': req.query, codes })
  res.json(codes)
})

app.listen(port, () => console.log(`listening on localhost:${port}`))