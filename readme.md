# node-tunnel - an http passtrough implemented in node

## install

```shell
$ git clone https://gitlab.com/opensas/node-tunnel.git
Cloning into 'node-tunnel'...
[...]

$ cd node-tunnel\

$ npm install
[...]
```

## run

```shell
$ npm run dev

> nodemon index.js

[nodemon] 1.19.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node index.js`
listening on localhost:8080
```

## test locally

on another temrinal

```shell
$ curl localhost:8080/codes
[]

$ curl localhost:8080/from_meli?code=new_code_01
{
  "code": "new_code_01",
  "destination": "http://localhost:8080/from_tunnel",
  "url": "http://localhost:8080/from_tunnel?code=new_code_01",
  "tunnel_response": [
    "new_code_01"
  ]
}

$ curl localhost:8080/from_meli?code=new_code_02
{
  "code": "new_code_02",
  "destination": "http://localhost:8080/from_tunnel",
  "url": "http://localhost:8080/from_tunnel?code=new_code_02",
  "tunnel_response": [
    "new_code_01",
    "new_code_02"
  ]
}

$ curl localhost:8080/codes
[
  "new_code_01",
  "new_code_02"
]
```

## deploy to now

Create an account at https://zeit.co

```shell

$ npm install -g now
> now@16.4.0 preinstall C:\data\devel\opt\node\node_modules\now
> node ./scripts/preinstall.js

+ now@16.4.0
updated 1 package in 5.909s

$ now

> Deploying C:\data\devel\apps\tmp\node-tunnel under opensas
> Using project node-tunnel
> Synced 1 file [764ms]
> https://node-tunnel-lvevsv1vb.now.sh [3s]
> Ready! Deployed to https://node-tunnel.opensas.now.sh [in clipboard] [16s]
```

## test on now

```shell
$ curl https://node-tunnel.opensas.now.sh/codes
[]

$ curl https://node-tunnel.opensas.now.sh/from_meli?code=new_code_01
{
  "code": "new_code_01",
  "destination": "http://https://node-tunnel.opensas.now.sh/from_tunnel",
  "url": "http://https://node-tunnel.opensas.now.sh/from_tunnel?code=new_code_01",
  "tunnel_response": [
    "new_code_01"
  ]
}

$ curl https://node-tunnel.opensas.now.sh/from_meli?code=new_code_02
{
  "code": "new_code_02",
  "destination": "http://https://node-tunnel.opensas.now.sh/from_tunnel",
  "url": "http://https://node-tunnel.opensas.now.sh/from_tunnel?code=new_code_02",
  "tunnel_response": [
    "new_code_01",
    "new_code_02"
  ]
}

$ curl https://node-tunnel.opensas.now.sh/codes
[
  "new_code_01",
  "new_code_02"
]
```
